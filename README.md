<p align="center">
    <img alt="GitHub issues" src="https://img.shields.io/github/issues/aoamusat/moneykarmasms">
    <img alt="Build Status" src="https://img.shields.io/bitbucket/pipelines/aoamusat/moneykarmasms/main">
</p>

## About
SMS API with features like caching, rate limiting.

## How to setup this project
* Clone this repository
* ```cd moneykarmasms```
* ```pip install -r requirements.txt```
* ```python manage.py makemigrations```
* ```python manage.py migrate```
* ```python manage.py runserver```

## Sample Data

## API Documentation
https://documenter.getpostman.com/view/1121711/UVyoXyUi
